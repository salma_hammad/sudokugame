﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sukuku
{
    class Generator
    {

        public int[,] Matrix = new int[9, 9];
        public int[,] tmp = new int[9, 9];

        Easy E = new Easy();
        Hard H = new Hard();
        Complex C = new Complex();

        public void FunctionOne(int M)
        {
            if (M == 0)
            {
                E.matrix();
                Matrix = E.Sodukou;
            }
            else if (M == 1)
            {
                H.matrix();
                Matrix = H.Sodukou;
            }
            else if (M == 2)
            {
                C.matrix();
                Matrix = C.Sodukou;
            }

            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    if (Matrix[i, j] == 9)
                        Matrix[i, j] = 1;

                    else if (Matrix[i, j] == 1)
                        Matrix[i, j] = 9;
                }
            }

        }

        public void FunctionTwo(int M)
        {
            if (M == 0)
            {
                E.matrix();
                Matrix = E.Sodukou;
            }
            else if (M == 1)
            {
                H.matrix();
                Matrix = H.Sodukou;
            }
            else if (M == 2)
            {
                C.matrix();
                Matrix = C.Sodukou;
            }

            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    if (j == 0)
                    {
                        tmp[i, j] = Matrix[i, j];
                        Matrix[i, j] = Matrix[i, j + 2];
                        Matrix[i, j + 2] = tmp[i, j];
                    }
                    else if (j == 3)
                    {
                        {
                            tmp[i, j] = Matrix[i, j];
                            Matrix[i, j] = Matrix[i, j + 2];
                            Matrix[i, j + 2] = tmp[i, j];
                        }
                    }
                    else if (j == 6)
                    {
                        tmp[i, j] = Matrix[i, j];
                        Matrix[i, j] = Matrix[i, j + 2];
                        Matrix[i, j + 2] = tmp[i, j];
                    }
                }
            }
        }

        /* public void FunctionThree(int M)
         {
             if (M == 0)
             {
                 E.matrix();
                 Matrix = E.Sodukou;
             }
             else if (M == 1)
             {
                 H.matrix();
                 Matrix = H.Sodukou;
             }
             else if (M == 2)
             {
                 C.matrix();
                 Matrix = C.Sodukou;
             }
             for (int i = n-1; i>=0 ; i--) 
             {
                 for (int j = 0; j < n ; j++) 
                 {
                        /* tmp[i, j] = Matrix[i, j];
                         Matrix[j, (n - 1) - i] = Matrix[i, j];
                         Matrix[j, (n - 1) - i] = tmp[i, j];
                 }
             }*/


        //not working correctly !
        /*public void FunctionFour(int M)
        {
            int n = 9;
            if (M == 0)
            {
                E.matrix();
                Matrix = E.Sodukou;
            }
            else if (M == 1)
            {
                H.matrix();
                Matrix = H.Sodukou;
            }
            else if (M == 2)
            {
                C.matrix();
                Matrix = C.Sodukou;
            }
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    Matrix[i, j] = Matrix[j, i];
                 /*   tmp[i, j] = Matrix[i, j];
                    Matrix[i, j] = Matrix[8 - j , i];
                   
                }
            }
        }*/

        public bool Comapare(int[,] matrix, int C, int M)
        {
            for (int j = 0; j < 9; j++)
            {
                for (int i = 0; i < 9; i++)
                {
                    if (C == 2)
                    {
                        FunctionOne(M);
                        if (Matrix[j, i] != matrix[j, i])
                            return false;
                    }
                    else if (C == 3)
                    {
                        FunctionTwo(M);
                        if (Matrix[j, i] != matrix[j, i])
                            return false;
                    }
                    /*  else if (C == 4)
                      {
                          FunctionThree(M);
                          if (Matrix[j, i] != matrix[j, i])
                              return false;
                      }
                      else if (C == 5)
                      {
                          FunctionFour(M);
                          if (Matrix[j, i] != matrix[j, i])
                              return false;
                      }*/
                }
            }
            return true;
        }

        public int[,] Solve(int[,] matrix, int C, int M)
        {
            if (C == 2)
            {
                FunctionOne(M);
                for (int i = 0; i < 9; i++)
                {
                    for (int j = 0; j < 9; j++)
                    {

                        tmp[i, j] = Matrix[i, j];
                    }
                }
            }
            else if (C == 3)
            {
                FunctionTwo(M);
                for (int i = 0; i < 9; i++)
                {
                    for (int j = 0; j < 9; j++)
                    {

                        tmp[i, j] = Matrix[i, j];
                    }
                }
            }
            /*else if (C == 4)
            {
                FunctionThree(M);
                for (int i = 0; i < 9; i++)
                {
                    for (int j = 0; j < 9; j++)
                    {
                            
                        tmp[i, j] = Matrix[i, j];
                    }
                }
            }
            else if (C == 5)
            {
                FunctionFour(M);
                for (int i = 0; i < 9; i++)
                {
                    for (int j = 0; j < 9; j++)
                    {
                        tmp[i, j] = Matrix[i, j];
                    }
                }
            }*/


            return tmp;
        }

    }

}
             


