﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sukuku
{
    //abstract class
    public abstract class Original 
    {

        public int[,] Sodukou = new int[9, 9];
        public bool[,] Checked = new bool[9, 9];
        
        //abstract Method
        abstract public void Rand();
        
   
        public void  matrix()
        {
            Sodukou[0, 0] = 1;
            Sodukou[0, 1] = 2;
            Sodukou[0, 2] = 3;
            Sodukou[0, 3] = 4;
            Sodukou[0, 4] = 5;
            Sodukou[0, 5] = 6;
            Sodukou[0, 6] = 7;
            Sodukou[0, 7] = 8;
            Sodukou[0, 8] = 9;

            Sodukou[1, 0] = 4;
            Sodukou[1, 1] = 5;
            Sodukou[1, 2] = 6;
            Sodukou[1, 3] = 7;
            Sodukou[1, 4] = 8;
            Sodukou[1, 5] = 9;
            Sodukou[1, 6] = 1;
            Sodukou[1, 7] = 2;
            Sodukou[1, 8] = 3;

            Sodukou[2, 0] = 7;
            Sodukou[2, 1] = 8;
            Sodukou[2, 2] = 9;
            Sodukou[2, 3] = 1;
            Sodukou[2, 4] = 2;
            Sodukou[2, 5] = 3;
            Sodukou[2, 6] = 4;
            Sodukou[2, 7] = 5;
            Sodukou[2, 8] = 6;

            Sodukou[3, 0] = 2;
            Sodukou[3, 1] = 1;
            Sodukou[3, 2] = 4;
            Sodukou[3, 3] = 3;
            Sodukou[3, 4] = 6;
            Sodukou[3, 5] = 5;
            Sodukou[3, 6] = 8;
            Sodukou[3, 7] = 9;
            Sodukou[3, 8] = 7;
            
            Sodukou[4, 0] = 3;
            Sodukou[4, 1] = 6;
            Sodukou[4, 2] = 5;
            Sodukou[4, 3] = 8;
            Sodukou[4, 4] = 9;
            Sodukou[4, 5] = 7;
            Sodukou[4, 6] = 2;
            Sodukou[4, 7] = 1;
            Sodukou[4, 8] = 4;

            Sodukou[5, 0] = 8;
            Sodukou[5, 1] = 9;
            Sodukou[5, 2] = 7;
            Sodukou[5, 3] = 2;
            Sodukou[5, 4] = 1;
            Sodukou[5, 5] = 4;
            Sodukou[5, 6] = 3;
            Sodukou[5, 7] = 6;
            Sodukou[5, 8] = 5;

            Sodukou[6, 0] = 5;
            Sodukou[6, 1] = 3;
            Sodukou[6, 2] = 1;
            Sodukou[6, 3] = 6;
            Sodukou[6, 4] = 4;
            Sodukou[6, 5] = 2;
            Sodukou[6, 6] = 9;
            Sodukou[6, 7] = 7;
            Sodukou[6, 8] = 8;

            Sodukou[7, 0] = 6;
            Sodukou[7, 1] = 4;
            Sodukou[7, 2] = 2;
            Sodukou[7, 3] = 9;
            Sodukou[7, 4] = 7;
            Sodukou[7, 5] = 8;
            Sodukou[7, 6] = 5;
            Sodukou[7, 7] = 3;
            Sodukou[7, 8] = 1;

            Sodukou[8, 0] = 9;
            Sodukou[8, 1] = 7;
            Sodukou[8, 2] = 8;
            Sodukou[8, 3] = 5;
            Sodukou[8, 4] = 3;
            Sodukou[8, 5] = 1;
            Sodukou[8, 6] = 6;
            Sodukou[8, 7] = 4;
            Sodukou[8, 8] = 2;
        }

 
    }
}
