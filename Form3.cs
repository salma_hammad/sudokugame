﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sukuku
{
    public partial class Form3 : Form
    {
        Generator Gener = new Generator();
        TextBox[,] Sodukou = new TextBox[9,9];
        bool[,] Checked = new bool[9,9];
        int[,] matrix = new int[9, 9];
        int[,] Compare = new int[9, 9];
        Easy E = new Easy();
        Hard H = new Hard();
        Complex C = new Complex();
        public int M;
        private int counter = 0;
        public int Score;
        
        //polymorphism
        public void Display()
        {
            MessageBox.Show("Your Name is: " + textBox82.Text 
                + "\n" + "Your Score is: " + Score);
            textBox82.ReadOnly = true;
        }

        //encapsolation 
        public int GetCounter()
        {
            return counter;
        }
        public void SetCounter(int C)
        {
            C = counter;
        }

        public Form3()
        {
            InitializeComponent();
          
            Sodukou[0,0] = textBox1;
            Sodukou[0,1] = textBox2;
            Sodukou[0,2] = textBox3;
            Sodukou[0,3] = textBox4;
            Sodukou[0,4] = textBox5;
            Sodukou[0,5] = textBox6;
            Sodukou[0,6] = textBox7;
            Sodukou[0,7] = textBox8;
            Sodukou[0,8]=  textBox9;

            Sodukou[1,0] = textBox18;
            Sodukou[1,1] = textBox17;
            Sodukou[1,2] = textBox16;
            Sodukou[1,3] = textBox15;
            Sodukou[1,4] = textBox14;
            Sodukou[1,5] = textBox13;
            Sodukou[1,6] = textBox12;
            Sodukou[1,7] = textBox11;
            Sodukou[1,8] = textBox10;

            Sodukou[2,0] = textBox27;
            Sodukou[2,1] = textBox26;
            Sodukou[2,2] = textBox25;
            Sodukou[2,3] = textBox24;
            Sodukou[2,4] = textBox23;
            Sodukou[2,5] = textBox22;
            Sodukou[2,6] = textBox21;
            Sodukou[2,7] = textBox20;
            Sodukou[2,8] = textBox19;

            Sodukou[3,0] = textBox36;
            Sodukou[3,1] = textBox35;
            Sodukou[3,2] = textBox34;
            Sodukou[3,3] = textBox33;
            Sodukou[3,4] = textBox32;
            Sodukou[3,5] = textBox31;
            Sodukou[3,6] = textBox30;
            Sodukou[3,7] = textBox29;
            Sodukou[3,8] = textBox28;
            
            Sodukou[4,0] = textBox45;
            Sodukou[4,1] = textBox44;
            Sodukou[4,2] = textBox43;
            Sodukou[4,3] = textBox42;
            Sodukou[4,4] = textBox41;
            Sodukou[4,5] = textBox40;
            Sodukou[4,6] = textBox39;
            Sodukou[4,7] = textBox38;
            Sodukou[4,8] = textBox37;

            Sodukou[5,0] = textBox54;
            Sodukou[5,1] = textBox53;
            Sodukou[5,2] = textBox52;
            Sodukou[5,3] = textBox51;
            Sodukou[5,4] = textBox50;
            Sodukou[5,5] = textBox49;
            Sodukou[5,6] = textBox48;
            Sodukou[5,7] = textBox47;
            Sodukou[5,8] = textBox46;

            Sodukou[6,0] = textBox63;
            Sodukou[6,1] = textBox62;
            Sodukou[6,2] = textBox61;
            Sodukou[6,3] = textBox60;
            Sodukou[6,4] = textBox59;
            Sodukou[6,5] = textBox58;
            Sodukou[6,6] = textBox57;
            Sodukou[6,7] = textBox56;
            Sodukou[6,8] = textBox55;

            Sodukou[7,0] = textBox72;
            Sodukou[7,1] = textBox71;
            Sodukou[7,2] = textBox70;
            Sodukou[7,3] = textBox69;
            Sodukou[7,4] = textBox68;
            Sodukou[7,5] = textBox67;
            Sodukou[7,6] = textBox66;
            Sodukou[7,7] = textBox65;
            Sodukou[7,8] = textBox64;

            Sodukou[8,0] = textBox81;
            Sodukou[8,1] = textBox80;
            Sodukou[8,2] = textBox79;
            Sodukou[8,3] = textBox78;
            Sodukou[8,4] = textBox77;
            Sodukou[8,5] = textBox76;
            Sodukou[8,6] = textBox75;
            Sodukou[8,7] = textBox74;
            Sodukou[8,8] = textBox73;
           
        }
        
     private void textBox9_TextChanged(object sender, EventArgs e)
        {
        }
        private void textBox8_TextChanged(object sender, EventArgs e)
        {
        }
        private void textBox7_TextChanged(object sender, EventArgs e)
        {
        }
        private void textBox6_TextChanged(object sender, EventArgs e)
        {
        }
        private void textBox5_TextChanged(object sender, EventArgs e)
        {
        }
        private void textBox4_TextChanged(object sender, EventArgs e)
        {
        }
        private void textBox3_TextChanged(object sender, EventArgs e)
        {
        }
        private void textBox2_TextChanged(object sender, EventArgs e)
        {
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
        }


        private void Form3_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        { 
            if (counter == 3)
                counter = 2;
            else
                counter++;

            Font myFont = new Font("Monotype Corvisa", 18.0f, FontStyle.Bold);
            Font font2 = new Font("Matura MT Script Capitals", 18f, FontStyle.Regular);

            for (int j = 0; j < 9; j++)
            {
                for (int i = 0; i < 9; i++)
                {
                    Sodukou[j, i].ReadOnly = false;
                }
            }
            
            if (counter == 2)
            {
                Gener.FunctionOne(M);
                matrix = Gener.Matrix;
            }
            else if (counter==3)
            {
                Gener.FunctionTwo(M);
                matrix = Gener.Matrix;
                //counter = 0;
            }
           /* else if (counter == 4)
            {
                Gener.FunctionThree(M);
                matrix = Gener.Matrix;
            }
             //there is a problem in here
            else if (counter == 2)
            {
                Gener.FunctionFour(M);
                matrix = Gener.Matrix;
                //counter = 0;
            }
            else
            {
                E.matrix();
                matrix = E.Sodukou;
            }*/
            for (int j = 0; j < 9; j++)
            {
                for (int i = 0; i < 9; i++)
                {
                    Checked[j, i] = true;
                }
            }

            if (M==0)
            {
                E.Rand();
                Checked = E.Checked;
            }
            else if (M==1)
            {
                H.Rand();
                Checked = H.Checked;
            }
            else if (M==2)
            {
                C.Rand();
                Checked = C.Checked;
            }
          

            for (int j = 0; j < 9; j++)
            {
                for (int i = 0; i < 9; i++)
                {
                    int xx = matrix[j, i];
                    if (Checked[j, i])
                    {

                        Sodukou[j, i].Text = xx.ToString();
                        Sodukou[j, i].ForeColor = Color.Black;
                        Sodukou[j, i].Font = new Font(Sodukou[j, i].Font, FontStyle.Regular);
                        Sodukou[j, i].Font = font2;
                        Sodukou[j, i].ReadOnly = true;

                    }
                    else
                    {
                        Sodukou[j, i].Text = "";
                        Sodukou[j, i].ForeColor = Color.DarkCyan;
                        Sodukou[j, i].Font = new Font(Sodukou[j, i].Font, FontStyle.Regular);
                    }
                }


            }
                       
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (counter == 0||counter==1)
            {
                MessageBox.Show("Not Correct");

            }
            else
            {
                for (int i = 0; i < 9; i++)
                {
                    for (int j = 0; j < 9; j++)
                    {

                        if (Sodukou[i, j].Text.ToString() != "")
                        {
                            Compare[i, j] = Convert.ToInt32(Sodukou[i, j].Text.ToString());
                        }
                        else
                        {
                            Compare[i, j] = 0;
                        }

                    }
                }

                bool check = false;
                check = Gener.Comapare(Compare, counter, M);
                if (check)
                {
                    //problem here
                    MessageBox.Show("Perfect ^_^ ");
                    Score += 5;
                    check = false;
                    Display();
                }
                else
                    MessageBox.Show("Not Correct");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (counter == 0||counter==1)
            {
                MessageBox.Show("Wrong Move");
            }
            else
            {
                int x;
                matrix = Gener.Solve(matrix, counter, M);
                for (int i = 0; i < 9; i++)
                {
                    for (int j = 0; j < 9; j++)
                    {

                        x = matrix[i, j];
                        Sodukou[i, j].Text = x.ToString();
                    }
                }

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
           
            CopyEasy F = new CopyEasy();
            F.Show();
            this.Close();
        }

        private void textBox2_TextChanged_1(object sender, EventArgs e)
        {
        }

        private void textBox9_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void textBox19_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox18_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox28_TextChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            Score = 0;
            this.Close();
        }

        private void textBox65_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox66_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox67_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox68_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox69_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox70_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox71_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox72_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox46_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox47_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox48_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox49_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox50_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox51_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox52_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox53_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox54_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox29_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox30_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox31_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox32_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox33_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox34_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox35_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox36_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox64_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox10_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox11_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox12_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox13_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox14_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox15_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox16_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox17_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox82_TextChanged(object sender, EventArgs e)
        {
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
       
    }
}
